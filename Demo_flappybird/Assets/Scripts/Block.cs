using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

public class Block : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public float distanceMin;
    [SerializeField] public float distanceMax;
    [SerializeField] public float nextBlockDistance;
    [SerializeField] public bool outOfCamera;
    public bool isRoot;
    public bool hasPass;
    private float maxWeight;
    [SerializeField]
    public GameObject TopBlock;
    [SerializeField]
    public GameObject BotBlock;

    private void Awake()
    {
        //TopBlock = GetComponentInChildren<Block>().GameObject.Find("Top"));
        //BotBlock = GameObject.Find("Bot");
    }

    void Start()
    {
        if (isRoot)
        {
            RandomTopBotBlock();
        }
            
    }

    // Update is called once per frame
    void Update()
    {
        CountScore();
    }
    

    public void RandomPosition(float nextblock)
    {
        float x = GameController.instance.previousBlock.transform.position.x;
        float y = GameController.instance.previousBlock.transform.position.y;
        
        transform.position = new Vector3(x + nextblock , y , 0);
        RandomTopBotBlock();
        GameController.instance.previousBlock = this;
    }

    public void RandomPosition(float x, float y)
    {
        transform.position = new Vector3(x, y, 0);
        RandomTopBotBlock();
    }

    public void RandomTopBotBlock()
    {
        float topY = Random.Range(8, 16f);
        float randomDistance = Random.Range(distanceMin, distanceMax);

        var transformPositionTop = TopBlock.transform.localPosition;
        Vector3 changeTop = new Vector3(transformPositionTop.x, topY, 0);

        TopBlock.transform.localPosition = changeTop;
        
        var transformPositionBot = TopBlock.transform.localPosition;
        Vector3 changeBot = new Vector3(transformPositionBot.x, topY - randomDistance, 0);
        BotBlock.transform.localPosition = changeBot;
    }

    public void CountScore()
    {
        if (hasPass) return;
        if (transform.position.x < GameController.instance.Player.transform.position.x)
        {
            GameController.instance.Point++;
            GameController.instance.dataPoint.ResetScore();
            float y = (TopBlock.transform.position.y + BotBlock.transform.position.y) / 2;
            Vector2 resp = new Vector2(transform.position.x, y); 
            GameController.instance.respawnPoint = resp;
            hasPass = true;
            if (GameController.instance.Point == GameController.instance.maxRangePoint)
            {
                //maxRangePoint += 1;
                GameController.instance.gameStatus = "GameWin";
                GameController.instance.MenuManager.GetStatusMenu();
                Time.timeScale = 0;
            }
        }

    }

}
