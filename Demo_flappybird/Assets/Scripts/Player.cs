using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public float Speed;
    [SerializeField] public float HeightForce;
    public Rigidbody2D rb;
    [SerializeField]
    public GameObject[] Heartbar;
    public int Heart;
    public Vector2 gravity;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = 0;
        CheckJumpsGravity();
        Heart = 3;
        gravity = Physics2D.gravity;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.instance.isPlayerControl || GameController.instance.gameStatus == "Ready")
        {
            if(GameController.instance.gameStatus != "Replay" && GameController.instance.gameStatus != "Ready")
                AutoMove();
            Jumps();
            CheckFall();
        }
        
    }

    private void FixedUpdate()
    {

    }

    public void AutoMove()
    {
        Vector2 Movement =  Vector2.right * Speed * Time.deltaTime;
        transform.Translate( Movement);
    }

    public void Jumps()
    {
        CheckJumpsGravity();
        
#if UNITY_EDITOR

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(GameController.instance.gameStatus == "Ready")
                GameController.instance.StartGame();
            Vector2 force = Vector2.up * HeightForce;
            rb.velocity = force;
        }
#endif
#if UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount > 0)
        {
            if(GameController.instance.gameStatus == "Ready")
                GameController.instance.StartGame();
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                Vector2 force = Vector2.up * HeightForce;
                rb.velocity = force;
            }
        }
#endif

    }

    public void CheckJumpsGravity()
    {
        if (rb.gravityScale == 1) return;
        if (!GameController.instance.isPlayerControl)
        {
            rb.gravityScale = 0;
            rb.velocity = new Vector2(0,0);
        }

        else
        {
            rb.gravityScale = 1;
        }
            
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Block"))
        {
            //GameController.instance.respawnPoint = GameController.instance.root.transform.position;
            GameController.instance.gameStatus = "lose";
            GameController.instance.isPlayerControl = false;
            rb.gravityScale = 0;
            rb.velocity = new Vector2(0,0);
            CheckLogic();
        }
        
    }

    public void CheckFall()
    {
        if ((GameController.instance.gameStatus.Contains("ingame") ||GameController.instance.gameStatus.Contains("Ready")) && Mathf.Abs(GameController.instance.root.BotBlock.transform.position.y) - Mathf.Abs(transform.position.y) < 0 
            && transform.position.y < GameController.instance.root.BotBlock.transform.position.y)
        {
            GameController.instance.gameStatus = "lose";
            CheckLogic();
        }
        
    }

    public void CheckLogic()
    {
        if (GameController.instance.Player.Heart - 1 == 0)
        {
            GameController.instance.gameStatus = "GameOver";
            GameController.instance.SetGameStatusLogic();
            Time.timeScale = 0f;
            return;
        }
        if (GameController.instance.gameStatus == "lose")
        {
            GameController.instance.SetGameStatusLogic();
            Time.timeScale = 0f;
            return;
        }
        GameController.instance.SetGameStatusLogic();
        Time.timeScale = 0f;
    }
}
