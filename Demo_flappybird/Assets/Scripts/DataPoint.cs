using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class DataPoint : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public Text ScoreBox;
    public int Score = 0;
    public TextAsset DataJson;
    string saveFile;

    [Serializable]
    public class HighScore
    {
        public int Score;

        public HighScore(int score)
        {
            Score = score;
        }
    }

    public HighScore _highScore;

    public void Awake()
    {
        _highScore = new HighScore(10);
        saveFile = DataJson.name + ".json";
        LoadJsonData();
    }

    void Start()
    {
        ResetScore();
    }

    // Update is called once per frame
    void Update()
    {
        //ScoreBox.text = GameController.instance.Point + "/" + _highScore.Score;
    }

    public void ResetScore()
    {
        ScoreBox.text = GameController.instance.Point + "";
        GameController.instance.dataPoint.Score = GameController.instance.Point;
    }

    public void LoadJsonData()
    {
        string json = ReadDataJson(saveFile);
        JsonUtility.FromJsonOverwrite(json, _highScore);
        //GameController.instance.maxRangePoint = _highScore.Score;
    }

    public void SaveJsonData()
    {
        string jsonString = JsonUtility.ToJson(_highScore);
        WriteDataJson(saveFile, jsonString);
        Debug.LogError(jsonString);
    }
    

    public void WriteDataJson(string fileName, string json)
    {
        string path = GetFilePath(fileName);
        File.WriteAllText($"{path}/{saveFile}", json);
    }

    public string ReadDataJson(string fileName)
    {
        string path = GetFilePath(fileName);
        return  File.ReadAllText($"{path}/{saveFile}");
    }

    private string GetFilePath(string saveFile)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            var path = Application.persistentDataPath + "/";
            CheckExist(path);
            return path;
        }

        Debug.Log(Application.dataPath);
        return Application.dataPath + "/Resources/";


    }
    
    public void CheckExist(string path)
    {
        if (Directory.Exists(path) == false)
        {
            Directory.CreateDirectory(path);
        }
    }
    
    
    
}
