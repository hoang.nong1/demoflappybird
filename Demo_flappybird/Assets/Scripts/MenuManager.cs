using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] public Canvas Replay;
    public SceneManager scene;
    public Button replay;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //GetStatusMenu();
    }

    public void GetStatusMenu()
    {
        if (GameController.instance.gameStatus.Contains("lose"))
        {
            Replay.gameObject.SetActive(true);
            GameController.instance.status.text = "Game Over";
        }
        if (GameController.instance.gameStatus.Contains("GameOver") || GameController.instance.gameStatus.Contains("GameWin"))
        {
            Replay.gameObject.SetActive(true);
            GameController.instance.status.text = GameController.instance.gameStatus;
            Debug.Log(GameController.instance.gameStatus);
            
        }
    }

    public void ButtonClicked()
    {
        replay.onClick.AddListener(HandleOnClick);
    }

    public void HandleOnClick()
    {
        if (GameController.instance.gameStatus.Contains("GameOver") ||
            GameController.instance.gameStatus.Contains("GameWin"))
        {
            Restart();
        }
        else if (GameController.instance.gameStatus.Contains("lose"))
            Replays();
    }
    
    public void Replays()
    {
        Time.timeScale = 1;
        GameController.instance.ReadyGame();
        Vector2 resp = GameController.instance.respawnPoint;
        //resp.x += 0.5f;
        GameController.instance.Player.transform.position = resp;
        
        GameController.instance.Player.Heartbar[GameController.instance.Player.Heart - 1].SetActive(false);
        GameController.instance.Player.Heart -= 1;
        //GameController.instance.Point -= 1;
        Replay.gameObject.SetActive(false);
    }

    public void Restart()
    {
        if (GameController.instance.gameStatus.Contains("GameWin"))
        {
            GameController.instance.dataPoint._highScore.Score = GameController.instance.maxRangePoint + 1;
            GameController.instance.dataPoint.SaveJsonData();
            GameController.instance.gameStatus = "Ready";
        }
        
        SceneManager.LoadSceneAsync("Game");
        
    }
    
    public void StartGame()
    {
        GameController.instance.isPlayerControl = true;
        GameController.instance.status.text = "";
        GameController.instance.gameStatus = "ingame";
        Time.timeScale = 1;
    }
}
