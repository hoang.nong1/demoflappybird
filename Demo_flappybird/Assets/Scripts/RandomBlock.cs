using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomBlock : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public Block rootBlock;
    public Queue<Block> BlockList;

    public Block PreviousBlock;
    // public Block previousBlock;
    public int Lenght;
    public float nextBlockDistance;
    
    void Start()
    {
        BlockList = new Queue<Block>();
        CreatBlockList();
    }

    // Update is called once per frame
    void Update()
    {
        SetNextBlock();
    }

    public void SetNextBlock()
    {
        if (BlockList.Peek().outOfCamera)
        {
            GameController.instance.nextBlock = BlockList.Peek();
            Block nextBlock = BlockList.Dequeue();
            nextBlock.hasPass = false;
            nextBlock.RandomPosition(nextBlockDistance);
            BlockList.Enqueue(nextBlock);
        }
    }

    public void CreatBlockList()
    {
        BlockList.Enqueue(rootBlock);
        GameController.instance.root = rootBlock;
        PreviousBlock = rootBlock;
        for (int i = 1; i <= Lenght; i++)
        {
            Block newBlock = Instantiate(PreviousBlock);
            newBlock.isRoot = false;
            newBlock.RandomPosition(PreviousBlock.transform.position.x + nextBlockDistance, PreviousBlock.transform.position.y);
            BlockList.Enqueue(newBlock);
            PreviousBlock = newBlock;
            if (i == Lenght)
                GameController.instance.previousBlock = newBlock;
        }
    }

}
