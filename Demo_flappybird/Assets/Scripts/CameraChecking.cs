using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChecking : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Camera mainCamera;
    [SerializeField] public RandomBlock RandomBlock;
    void Start()
    {
        mainCamera = gameObject.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckCar();
    }

    private void CheckCar()
    {
        var lineOutOfViewPosition = RandomBlock.BlockList.Peek().transform.position;
        var viewPos = mainCamera.WorldToViewportPoint(lineOutOfViewPosition);

        
        if (viewPos.x > 2 || viewPos.x < -1.5)
        {
            RandomBlock.BlockList.Peek().outOfCamera = true;
        }
        else
        {
            RandomBlock.BlockList.Peek().outOfCamera = false;
        }
        
        
    }
}
