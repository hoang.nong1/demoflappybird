using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    // Start is called before the first frame update
    public static GameController instance;
    public int Point;
    public DataPoint dataPoint;
    public Block previousBlock;
    public Block nextBlock;
    public Block root;
    [SerializeField] 
    public int maxRangePoint;
    public string gameStatus;
    public bool isPlayerControl = false;
    [SerializeField] public Text PointText;
    public MenuManager MenuManager;
    public Text status;
    public Vector2 respawnPoint;
    public Player Player;
    public string saveFile;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    void Start()
    {
        Point = 0;
        ReadyGame();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetGameStatusLogic()
    {
        MenuManager.GetStatusMenu();
    }

    public void StartGame()
    {
        instance.isPlayerControl = true;
        instance.status.text = "";
        instance.gameStatus = "ingame";
        maxRangePoint = dataPoint._highScore.Score;
        Time.timeScale = 1;
    }

    public void ReadyGame()
    {
        instance.isPlayerControl = false;
        instance.status.text = "Start";
        instance.gameStatus = "Ready";
        //instance.Player.rb.gravityScale = 0;
        //Time.timeScale = 0;
    }
    
    

}
